# Qbus SDK for PHP

### 如何使用
通过composer引用SDK
```javascript
"repositories": {
    "qbus": {
        "type": "vcs",
        "url": "https://git.kuainiujinke.com/finlab/qbus-sdk-php.git"
    }
},
"require": {
    "finlab/qbus-sdk-php": "dev-master"
}
```

### 开始使用
```php
<?php
$qbus = new Qbus($endPoint, $userName, $userToken, $timeoutSeconds);
```
参数：
* endPoint: Qbus Web Api的地址
* userName: 用户的用户名
* userToken: 用户授权token
* timeoutSeconds: 可选参数，请求QbusApi超时时间，默认为30秒

### endpoint 地址
```
dev环境:
* http://qbus-api-dev.kuainiujinke.com/qbus-api
* hosts:10.10.10.101 qbus-api-dev.kuainiujinke.com
```
test环境：
* http://qbus-api-test.kuainiujinke.com/qbus-api
* hosts:10.1.1.3  qbus-api-test.kuainiujinke.com

### 获取一个队列
```php
<?php
$queue = $qbus->getQueue($queueName);
```
参数：
* queueName：队列名称

返回值：
* 队列操作对象

### 发送消息到队列
特性：
* 支持批量发送 最大支持16条批量发送

```php
<?php
$msgId = $queue->sendMessage($message, $delaySeconds);
$msgId = $queue->sendBatchMessage($messages, $delaySeconds);
```
参数：
* message(s)：消息内容（列表），字符串类型
* delaySeconds：消息延迟投递时间（可选参数，消息会在延迟指定时间后被投递到队列）

返回值：
* 消息唯一标识

### 从队列拉消息
特性：
* 支持指定时长的长连接
* 支持批量拉取消息 最大支持16条批量消费 

```php
<?php
$msg = $queue->receiveMessage($pollingWaitSeconds);
$msgList = $queue->batchReceiveMessage($messageCount, $pollingWaitSeconds);
```
参数
* pollingWaitSeconds: 长连接等待时长（0~30秒）
* messageCount：批量拉取的消息数量

返回值：
* 消息（列表）, 消息对象中的各个字段说明，见[消息对象](https://git.kuainiujinke.com/finlab/qbus-api/wikis/%E6%B6%88%E6%81%AF%E5%AF%B9%E8%B1%A1)

### 确认消息
特性：
* 拉取消息后，消息会保持一段时间不可见(1s~1day)，消费端处理完后需要显式删除该消息，否则超时后，消息将会重新可见，导致重复消费。
* 支持批量确认 最大支持16条批量确认 

```php
$queue->deleteMessage($message);
$queue->deleteBatchMessage($messages);
```
参数：
* $message(s)：待确认的消息(列表)。

返回值：
* 无

### 获取一个主题
```php
<?php
$topic = $qbus->getTopic($topicName);
```
参数：
* topicName：主题名称

返回值：
* 主题操作对象

### 发布消息到主题
特性：
* 支持批量发布 最大支持16条批量发布 
* 支持消息tag，订阅端可按tag过滤消息
* 支持routing key，订阅端可按routing key过滤消息

```php
<?php
$msgId = $topic->publishMessage($message, $tagList, $routingKey, $delaySeconds);
$msgId = $topic->publishBatchMessage($messages, $tagList, $routingKey, $delaySeconds);
```
参数：
* message(s)：消息内容（列表），字符串类型
* tagList：消息标签（用于消息过滤)。标签数量不能超过5个，每个标签不超过16个字符。
* routingKey: 长度<=64字节，该字段用于表示发送消息的路由路径，最多含有15个“.”， 即最多16个词组。
* delaySeconds：消息延迟投递时间（可选参数，消息会在延迟指定时间后被投递到订阅方）

返回值：
* 消息唯一标识
