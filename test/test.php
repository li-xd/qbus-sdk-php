<?php
/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/4
 * Time: 上午11:30
 */
namespace Qbus;

include '../src/Qbus.php';
include '../src/QbusClient.php';
include '../src/Message.php';
include '../src/Queue.php';
include '../src/Topic.php';
include '../src/Exception/QbusException.php';
include '../src/Exception/QbusServerException.php';
include '../src/Exception/QbusClientException.php';
include '../src/Util/HttpUtils.php';
include '../src/Util/HttpResp.php';
include '../src/Util/HttpReq.php';
include '../src/Util/Signature.php';

$qbus = new Qbus('http://10.10.10.101:9000/qbus-api', 'test', '123456');

$queue = $qbus->getQueue('testQ');
$msgList = $queue->receiveBatchMessage(10, 30);
$queue->deleteBatchMessage($msgList);

$msgId = $queue->sendMessage('finlab', 15);
$msg = $queue->receiveMessage(30);
$queue->deleteMessage($msg);

$msgId = $queue->sendBatchMessage(array("john", "yang"), 15);
$msgList = $queue->receiveBatchMessage(10, 30);
$queue->deleteBatchMessage($msgList);

$topic = $qbus->getTopic('testT');
$msgId = $topic->publishMessage('John Yang', null, null, 30);
$msgId = $topic->publishBatchMessage(["john", "yang"]);
