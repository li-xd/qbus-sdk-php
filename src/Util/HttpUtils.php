<?php
namespace Qbus\Util;

use Qbus\Exception\QbusClientException;

class HttpUtils
{
    private $host;

    public function __construct($host)
    {
        $this->host = $host;
        $this->curl = null;

        if (isset($this->host) && strlen($this->host) > 0 && $this->host[strlen($this->host)-1] != '/') {
            $this->host .= '/';
        }
    }

    public function sendRequest($request, $userTimeout)
    {
        if ($this->curl == null) {
            $this->curl = curl_init();
        }

        if ($this->curl == null) {
            throw new QbusClientException("Curl init failed");
        }

        $url = $this->host.$request->path;
        if ($request->method == 'POST') {
            curl_setopt($this->curl, CURLOPT_POST, true);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $request->data);
        } else {
            curl_setopt($this->curl, CURLOPT_POST, false);
            $url .= '?' . $request->data;
        }

        if (isset($request->header)) {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $request->header);
        }

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $userTimeout);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

        if (false !== strpos($url, "https")) {
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
        }

        $resultStr = curl_exec($this->curl);
        if (curl_errno($this->curl)) {
            throw new QbusClientException(curl_error($this->curl));
        }

        $info = curl_getinfo($this->curl);
        $response = new HttpResp($info['http_code'], null, $resultStr);
        return $response;
    }
}
