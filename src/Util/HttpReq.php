<?php
/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/4
 * Time: 上午10:23
 */
namespace Qbus\Util;

class HttpReq
{
    public $header;
    public $method;
    public $path;
    public $data;

    public function __construct($method, $path, $header, $data)
    {
        $this->method = $method;
        $this->path = $path;
        $this->header = $header;
        $this->data = $data;
    }
}
