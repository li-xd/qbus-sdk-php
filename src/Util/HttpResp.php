<?php
/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/4
 * Time: 上午10:23
 */
namespace Qbus\Util;

class HttpResp
{
    public $header;
    public $status;
    public $data;

    public function __construct($status, $header, $data)
    {
        $this->status = $status;
        $this->header = $header;
        $this->data = $data;
    }
}
