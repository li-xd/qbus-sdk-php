<?php
namespace Qbus;

class Topic
{
    private $topicName;
    private $qbusClient;

    public function __construct($topicName, $cmqClient)
    {
        $this->topicName = $topicName;
        $this->qbusClient = $cmqClient;
    }
    
    public function publishMessage($message, array $tagList = null, $routingKey = null, $delaySeconds = 0)
    {
        $msgList = $this->publishBatchMessage(array($message), $tagList, $routingKey, $delaySeconds);
        return $msgList[0];
    }

    public function publishBatchMessage(array $messageList, array $tagList = null, $routingKey = null, $delaySeconds = 0)
    {
        return $this->qbusClient->publishTopicMessage($this->topicName, $messageList, $tagList, $routingKey, $delaySeconds);
    }
}
