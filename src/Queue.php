<?php
namespace Qbus;

class Queue
{
    private $queueName;
    private $qbusClient;

    public function __construct($queueName, $qbusClient)
    {
        $this->queueName = $queueName;
        $this->qbusClient = $qbusClient;
    }

    public function sendMessage($message, $delaySeconds = 0)
    {
        $msgIdList = $this->sendBatchMessage(array($message), $delaySeconds);
        return count($msgIdList) > 0 ? $msgIdList[0] : [];
    }

    public function sendBatchMessage(array $messages, $delaySeconds = 0)
    {
        return $this->qbusClient->sendMessage($this->queueName, $messages, $delaySeconds);
    }

    public function receiveMessage($pollingWaitSeconds = 30)
    {
        $msgList = $this->receiveBatchMessage(1, $pollingWaitSeconds);
        return count($msgList) > 0 ? $msgList[0] : [];
    }

    public function receiveBatchMessage($messageCount, $pollingWaitSeconds = 30)
    {
        return $this->qbusClient->receiveMessage($this->queueName, $messageCount, $pollingWaitSeconds);
    }

    public function deleteMessage($message)
    {
        $this->deleteBatchMessage(array($message));
    }

    public function deleteBatchMessage(array $messages)
    {
        $receiptHandleList = [];
        foreach ($messages as $message) {
            $receiptHandleList[] = $message->receiptHandle;
        }
        $this->qbusClient->deleteMessage($this->queueName, $receiptHandleList);
    }
}
