<?php
namespace Qbus\Exception;

/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/5
 * Time: 下午2:17
 */
class QbusServerException extends QbusException
{
    public $requestId;
    public function __construct($message, $code = -1, $data = array(), $requestId = null)
    {
        parent::__construct($message, $code, $data);
        $this->requestId = $requestId;
    }

    public function __toString()
    {
        return "QbusServerException  " .  $this->getInfo() . ", RequestID:" . $this->requestId;
    }
}
