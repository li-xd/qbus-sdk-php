<?php
namespace Qbus\Exception;

/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/5
 * Time: 下午2:15
 */
class QbusClientException extends QbusException
{
    public function __construct($message, $code = -1, $data = array())
    {
        parent::__construct($message, $code, $data);
    }

    public function __toString()
    {
        return "QbusClientException  " .  $this->getInfo();
    }
}
