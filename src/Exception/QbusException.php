<?php
namespace Qbus\Exception;

use RuntimeException;

/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/5
 * Time: 下午2:12
 */

class QbusException extends RuntimeException
{
    /*
    @type code: int
    @param code: 错误类型

    @type message: string
    @param message: 错误描述

    @type data: array
    @param data: 错误数据
    */

    public $code;
    public $message;
    public $data;

    public function __construct($message, $code = -1, $data = array(), $previousException = null)
    {
        parent::__construct($message, $code, $previousException);
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }

    public function __toString()
    {
        return "Qbus Exception\n" .  $this->getInfo();
    }

    public function getInfo()
    {
        $info = array(
            "code" => $this->code,
            "data" => $this->data,
            "message" => $this->message
        );
        return json_encode($info);
    }
}
