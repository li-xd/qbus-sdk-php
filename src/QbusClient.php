<?php
namespace Qbus;

use Qbus\Exception\QbusServerException;
use Qbus\Util\HttpReq;
use Qbus\Util\HttpUtils;

class QbusClient
{
    private $host;
    private $userName;
    private $userToken;
    private $http;
    private $timeoutSeconds;
    private $version = QBUS_SDK_VERSION;
    private $sign_method = 'HmacSHA1';
    private $pathDef = array(
        "ReceiveMessage" => array("GET", "queue/messages/pull"),
        "DeleteMessage" => array("POST", "queue/messages/ack"),
        "SendMessage" => array("POST", "queue/messages/send"),
        "PublishMessage" => array("POST", "topic/messages/publish"),
    );

    public function __construct($host, $userName, $userToken, $timeoutSeconds)
    {
        $this->host = $host;
        $this->userName = $userName;
        $this->userToken = $userToken;
        $this->timeoutSeconds = $timeoutSeconds;
        $this->http = new HttpUtils($this->host);
    }

    protected function checkStatus($resp)
    {
        if ($resp->status != 200) {
            $errorMsg = "Qbus api server response status code ".$resp->status;
            throw new QbusServerException($errorMsg, $resp->status);
        }

        $respData = json_decode($resp->data, true);
        $code = $respData['code'];
        $message = $respData['message'];
        $data = $respData['data'];
        $requestId = $respData['requestId'];

        if ($code != 0) {
            throw new QbusServerException($message, $code, $data, $requestId);
        }
    }

    protected function requestQbusApi($action, $params, $timeout = 0)
    {
        $method = $this->pathDef[$action][0];
        $path = $this->pathDef[$action][1];
        $header = array(
            "username:".$this->userName,
            "token:".$this->userToken,
            "sdk:".$this->version,
            //"Connection:Keep-Alive",
        );

        if ($method == 'GET') {
            $data = http_build_query($params);
        } else {
            $data = json_encode($params, JSON_UNESCAPED_UNICODE);
            $header[] = "Content-Type: application/json;charset='utf-8'";
        }

        //$signatureContent = Signature::makeSignPlainText($params, $method, $this->host, $path);
        //$header[] = "signature:".Signature::sign($signatureContent, $this->userToken, $this->sign_method);

        $req = new HttpReq($method, $path, $header, $data);
        $resp = $this->http->sendRequest($req, $timeout > 0 ? $timeout : $this->timeoutSeconds);

        return $resp;
    }

//===============================================queue operation===============================================

    public function sendMessage($queueName, $messages, $delaySeconds)
    {
        $params = array(
            'queueName' => $queueName,
            'delayTime' => $delaySeconds,
            'data' => $messages,
        );
        $resp = $this->requestQbusApi('SendMessage', $params);
        $this->checkStatus($resp);

        $ret = json_decode($resp->data, true);
        return $ret['data'];
    }

    public function receiveMessage($queueName, $numOfMsg, $pollingWaitSeconds)
    {
        $params = array(
            'queueName' => $queueName,
            'numOfMsg' => $numOfMsg,
            'pollingWaitSeconds' => $pollingWaitSeconds
        );

        $resp = $this->requestQbusApi('ReceiveMessage', $params, $pollingWaitSeconds + 10);
        $this->checkStatus($resp);

        $messages = [];
        $ret = json_decode($resp->data, true);
        foreach ($ret['data'] as $msg) {
            $message = new Message();
            $message->messageId = $msg['msgId'];
            $message->messageBody = $msg['msgBody'];
            $message->receiptHandle = $msg['receiptHandle'];
            $message->dequeueCount = $msg['dequeueCount'];
            $message->enqueueTime = $msg['enqueueTime'];
            $message->firstDequeueTime = $msg['firstDequeueTime'];
            $message->nextVisibleTime = $msg['nextVisibleTime'];
            $messages[] = $message;
        }
        return $messages;
    }

    public function deleteMessage($queueName, $receiptHandles)
    {
        $params = array(
            'queueName' => $queueName,
            'data' => $receiptHandles
        );

        $resp = $this->requestQbusApi('DeleteMessage', $params);
        $this->checkStatus($resp);
    }
    
 //=============================================topic operation================================================
    
    public function publishTopicMessage($topicName, $messageList, $tagList, $routingKey, $delaySeconds)
    {
        $params = array(
            'topicName' => $topicName,
            'data' => $messageList,
            'tagList' => $tagList,
            'routingKey' => $routingKey,
            'delayTime' => $delaySeconds
        );

        $resp = $this->requestQbusApi("PublishMessage", $params);
        $this->checkStatus($resp);
        $ret = json_decode($resp->data, true);
        return $ret['data'];
    }
}
