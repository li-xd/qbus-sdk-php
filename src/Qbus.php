<?php
namespace Qbus;

define('QBUS_SDK_VERSION', 'SDK_PHP_1.0.3');

class Qbus
{
    private $qbusClient;

    /**
     * Qbus constructor.
     * @param $endPoint string QbusApi地址
     * @param $userName string 用户名
     * @param $userToken string 用户token
     * @param int $timeoutSeconds 请求QbusApi超时时间，默认为30秒
     */
    public function __construct($endPoint, $userName, $userToken, $timeoutSeconds = 30)
    {
        $this->qbusClient = new QbusClient($endPoint, $userName, $userToken, $timeoutSeconds);
    }

    /**
     * 获取队列操作对象
     * @param $queueName string 队列名称
     * @return Queue 队列操作对象
     */
    public function getQueue($queueName)
    {
        return new Queue($queueName, $this->qbusClient);
    }

    /**
     * 获取主题操作对象
     * @param $topicName string 主题名称
     * @return Topic 主题操作对象
     */
    public function getTopic($topicName)
    {
        return new Topic($topicName, $this->qbusClient);
    }
}
