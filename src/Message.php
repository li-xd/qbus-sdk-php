<?php
/**
 * Created by PhpStorm.
 * User: yangzhyo
 * Date: 2017/9/7
 * Time: 下午3:11
 */

namespace Qbus;

class Message
{
    public $messageId;
    public $receiptHandle;
    public $messageBody;
    public $enqueueTime;
    public $nextVisibleTime;
    public $firstDequeueTime;
    public $dequeueCount;
}
